package ch.bbw.mashup;

/**
 * @author  Laurin Schneider
 * @version 1.0
 *
 */
public class Coordinate {
	private double xCoordinate;
	private double yCoordinate;
	
	
	public Coordinate(Double xCoordinate, Double yCoordinate) {
		
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		
	}
	public double getxCoordinate() {
		return xCoordinate;
	}
	public void setxCoordinate(double xCoordinate) {
		this.xCoordinate = xCoordinate;
	}
	public double getyCoordinate() {
		return yCoordinate;
	}
	public void setyCoordinate(double yCoordinate) {
		this.yCoordinate = yCoordinate;
	}
	
	
}
